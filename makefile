HOMEP = ${HOME}/Downloads/
INCS = -I${HOMEP}/boost_1_55_0/ 
LIBS = -lalglib -L${HOMEP}/Ipopt-debug/lib -L/usr/lib64 -lipopt -lblas -llapack -lm -ldl  -L/usr/lib/gcc/x86_64-redhat-linux/4.4.1 -lgfortranbegin -lgfortran -lfftw3l -lgcc_s
LIBS0 = -L${HOMEP}/lib/lib -lm -lfftw3l -lgfortranbegin -lgfortran -lgcc_s -lgmp
LIBS1 = -L${HOMEP}/lib/lib -lm -lgmp
LIBS2 = -lgfortranbegin -lgfortran -L${HOMEP}/lib -L${HOMEP}/lib/lib -lfftw3l -L${HOME}/gurobi605/linux64/lib -lgurobi_c++ -lgurobi60
OPTIONS = -O3 -DNDEBUG -std=c++0x
CC = g++

set-triple.o: set-triple.hpp set-triple.hpp 
$(CC) -c set-triple.cpp $(OPTIONS) $(INCS)

chebyshev.o: chebyshev.hpp chebyshev.cpp
$(CC) -c chebyshev.cpp $(OPTIONS) $(INCS)

chebyshev_q.o: chebyshev_q.hpp chebyshev_q.cpp
$(CC) -c chebyshev_q.cpp $(OPTIONS) $(INCS) -fext-numeric-literals


beta_der.o: beta_der.f
gfortran -c beta_der.f

compute.o: compute.cpp
$(CC) -c -g -std=c++11 ${INCS} compute.cpp  -fext-numeric-literals


testq: compute.o beta_der.o chebyshev_q.o set-triple.o
$(CC) -g -o testq  compute.o chebyshev_q.o set-triple.o beta_der.o -lm -lfftw3q -lfftw3l -lrt -lgmp -lgfortran -L/usr/local/lib -lquadmath -lboost_serialization
