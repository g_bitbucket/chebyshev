#include "chebyshev_q.hpp"

using boost::math::policies::policy;
using boost::math::policies::digits10;

using namespace std;
using namespace boost::multiprecision;
class result
{

        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive & ar, const unsigned int version){
        ar & vx;
        ar & omvx;
}
public:
        boost::multiprecision::mpf_float_100 vx,omvx; 
	result(mpf_float_100 _vx,mpf_float_100 _omvx):vx(_vx),omvx(_omvx){};
	result():vx(),omvx(){};
};

class myarchive
{
        friend class boost::serialization::access;
        template <class Archive>
        void serialize(Archive & ar, const unsigned int version){
        ar & aPrior;
        ar & bPrior;
        ar & nPoints;
        ar & index;
}
public:
        double aPrior,bPrior;
        int nPoints,index;
        myarchive(double a,double b,int npoints,int ind):aPrior(a),bPrior(b),nPoints(npoints),index(ind){};
        myarchive():aPrior(),bPrior(),nPoints(),index(){};
};
bool operator<(const myarchive& v1, const myarchive& v2)
{
  return (v1.aPrior< v2.bPrior)||
        ((v1.aPrior==v2.bPrior)&&(v1.nPoints<v2.nPoints))||
        ((v1.aPrior==v2.bPrior)&&(v1.nPoints==v2.nPoints)&&(v1.index < v2.index));
}

bool operator==(const myarchive& v1, const myarchive& v2)
{
  return ((v1.aPrior==v2.bPrior)&&(v1.nPoints==v2.nPoints)&&(v1.index==v2.index));
}

InDCT calcIn512(512);
InDCT calcIn1024(1024);
InDCT calcIn2048(2048);
InDCT calcIn4096(4096);
InDCT calcIn8192(8192);
InDCT calcIn16384(16384);
InDCT calcIn65536(65536);

//Find coefficients of the Chebyshev expansion
void findChebyshev(const vector<bdouble>& f, vector<bdouble>& c)
{
  const unsigned n = f.size() - 1;
  c.resize(n + 1);
  bdouble factor = 2.0 / (n + 1);
  for (unsigned k = 0; k <= n; k++) {
    bdouble sum = 0.0;
    for (unsigned j = 0; j <= n; j++) sum += f[j] * boost::math::cos_pi(k * (j + 0.5) / (n + 1));
    c[k] = factor * sum;
  }
}

//Evaluate Chebyshev sum by Clenshaw's method
mydouble evalChebyshev(mydouble x, const vector<mydouble>& c)
{
  const int n = c.size() - 1;
  if (n == 0) return 0.5 * c[0];
  mydouble y = 2 * x - 1;
  vector<mydouble> b(n + 2);
  b[n + 1] = 0; b[n] = c[n];
  for (int r = n - 1; r >= 1; r--) b[r] = 2 * y * b[r + 1] - b[r + 2] + c[r];
  return y * b[1] - b[2] + 0.5 * c[0];
}

//Evaluate Chebyshev sum by Clenshaw's method
//n represents the highest order of the term in the expansion
mydouble evalChebyshev(mydouble x, unsigned n, const mydouble* c)
{
  if (n == 0) return 0.5 * c[0];
  mydouble y = 2 * x - 1;
  //cout << y << " ";
  mydouble *b = new mydouble[n + 2];
  b[n + 1] = 0; b[n] = c[n];
  for (int r = n - 1; r >= 1; r--) {
    b[r] = 2 * y * b[r + 1] - b[r + 2] + c[r];
    //cout << c[r] << " " << b[r] << " ";
  }
  mydouble value = y * b[1] - b[2] + 0.5 * c[0];
  //cout << endl << value << endl;
  delete[] b;
  return value;
}
bdouble evalChebyshev(bdouble x, const vector<bdouble>& c)
{
  const int n = c.size() - 1;
  if (n == 0) return 0.5 * c[0];
  bdouble y = 2 * x - 1;
  vector<bdouble> b(n + 2);
  b[n + 1] = 0; b[n] = c[n];
  for (int r = n - 1; r >= 1; r--) b[r] = 2 * y * b[r + 1] - b[r + 2] + c[r];
  return y * b[1] - b[2] + 0.5 * c[0];
}

//Evaluate Chebyshev sum by Clenshaw's method
//n represents the highest order of the term in the expansion
bdouble evalChebyshev(bdouble x, unsigned n, const bdouble* c)
{
  if (n == 0) return 0.5 * c[0];
  bdouble y = 2 * x - 1;
  //cout << y << " ";
  bdouble *b = new bdouble[n + 2];
  b[n + 1] = 0; b[n] = c[n];
  for (int r = n - 1; r >= 1; r--) {
    b[r] = 2 * y * b[r + 1] - b[r + 2] + c[r];
    //cout << c[r] << " " << b[r] << " ";
  }
  bdouble value = y * b[1] - b[2] + 0.5 * c[0];
  //cout << endl << value << endl;
  delete[] b;
  return value;
}

void intChebyshev(const vector<mydouble>& c, vector<mydouble>& cInt)
{
  short sign = 1;
  const int n = c.size() - 1; //Highest order term in the expansion represented by c
  mydouble sum = 0;
  //cInt.resize(n + 2); // n + 2 terms in cInt
  cInt.resize(n + 1); // n + 1 for now -- do not increase the max degree of the expansion
  for (int i = 1; i <= n - 1; i++) {
    cInt[i] = 0.25 * (c[i - 1] - c[i + 1]) / i;
    sum += sign * cInt[i];
    sign = -sign;
  }
  cInt[n] = 0.25 * c[n - 1] / n;
  //cInt[n + 1] = 0.25 * c[n] / (n + 1);
  //sum += sign * (cInt[n] - cInt[n + 1]);
  sum += sign * cInt[n];
  cInt[0] = 2 * sum;
}

//n is the highest order term in the expansion represented by c
void intChebyshev(int n, const mydouble* c, mydouble* cInt)
{
  short sign = 1;
  mydouble sum = 0;
  for (int i = 1; i <= n - 1; i++) {
    cInt[i] = 0.25 * (c[i - 1] - c[i + 1]) / i;
    sum += sign * cInt[i];
    sign = -sign;
  }
  cInt[n] = 0.25 * c[n - 1] / n;
  sum += sign * cInt[n];
  cInt[0] = 2 * sum;
}
void intChebyshev(const vector<bdouble>& c, vector<bdouble>& cInt)
{
  short sign = 1;
  const int n = c.size() - 1; //Highest order term in the expansion represented by c
  bdouble sum = 0;
  //cInt.resize(n + 2); // n + 2 terms in cInt
  cInt.resize(n + 1); // n + 1 for now -- do not increase the max degree of the expansion
  for (int i = 1; i <= n - 1; i++) {
    cInt[i] = 0.25 * (c[i - 1] - c[i + 1]) / i;
    sum += sign * cInt[i];
    sign = -sign;
  }
  cInt[n] = 0.25 * c[n - 1] / n;
  //cInt[n + 1] = 0.25 * c[n] / (n + 1);
  //sum += sign * (cInt[n] - cInt[n + 1]);
  sum += sign * cInt[n];
  cInt[0] = 2 * sum;
}

//n is the highest order term in the expansion represented by c
void intChebyshev(int n, const bdouble* c, bdouble* cInt)
{
  short sign = 1;
  bdouble sum = 0;
  for (int i = 1; i <= n - 1; i++) {
    cInt[i] = 0.25 * (c[i - 1] - c[i + 1]) / i;
    sum += sign * cInt[i];
    sign = -sign;
  }
  cInt[n] = 0.25 * c[n - 1] / n;
  sum += sign * cInt[n];
  cInt[0] = 2 * sum;
}

//Evaluate one LL term given by parameters a and b at the roots of T_n(x)
void evalLLTermAtRoots(unsigned n, double a, double b, vector<bdouble>& f) 
{
  f.resize(n + 1);
  for (unsigned j = 0; j <= n; j++) {
    bdouble x = 0.5 * (boost::math::cos_pi((j + 0.5) / (n + 1)) + 1.0);
    f[j] = (boost::math::powm1(x, a - 1.0) + 1.0) * (boost::math::powm1(1.0 - x, b - 1.0) + 1.0);
  }
}


//Evaluate one LL term times Chebyshev at the roots of T_n(x)
void evalLLTermTimesChebyshevAtRoots(unsigned n, double a, double b, const vector<bdouble>& c, vector<bdouble>& f) 
{
  f.resize(n + 1);
  for (unsigned j = 0; j <= n; j++) {
    bdouble x = 0.5 * (boost::math::cos_pi((j + 0.5) / (n + 1)) + 1.0);
    f[j] = (boost::math::powm1(x, a - 1.0) + 1.0) * (boost::math::powm1(1.0 - x, b - 1.0) + 1.0) * evalChebyshev(x, c);
  }
}

InDCT::InDCT(unsigned nP, bool lookupTable) 
  : nPoints(nP), numCalls(0), totalClocks(0), numLookups(0), lookupClocks(0), doLookup(lookupTable), abToI(),
    logPriorFactor(0.0), priorSet(false), useIBeta(false), debugOut(false), 
    aPreset(0.0), bPreset(0.0)
{
  assert(nPoints > 0);
  x = new bdouble[nPoints]; //This one is not used inside FFT subroutines
  omx = new bdouble[nPoints]; //This one is not used inside FFT subroutines
  logx = new mydouble[nPoints]; //This one is not used inside FFT subroutines
  logomx = new mydouble[nPoints]; //This one is not used inside FFT subroutines
  isZero = new bool[nPoints];
  isOne  = new bool[nPoints];
  f = (mydouble*) fftwq_malloc ( sizeof (mydouble) * nPoints);
  fNext = (mydouble*) fftwq_malloc ( sizeof (mydouble) * nPoints);
  c = (mydouble*) fftwq_malloc ( sizeof (mydouble) * nPoints);
  cInt = (mydouble*) fftwq_malloc ( sizeof (mydouble) * nPoints);

  plan_forward  = fftwq_plan_r2r_1d(nPoints, f,    c,     FFTW_REDFT10, FFTW_ESTIMATE); 
  plan_backward = fftwq_plan_r2r_1d(nPoints, cInt, fNext, FFTW_REDFT01, FFTW_ESTIMATE); 
    
  //if (debugOut) cout << nPoints << endl;
  for (int i = 0; i < nPoints; i++) {
    bdouble vx = 0.5 * (boost::math::cos_pi<bdouble>((i + 0.5) / nPoints) + 1.0);
    //0.5 * (cos(pi * (i + 0.5) / nPoints) + 1.0); //Scaled to unterval from 0 to 1 
    x[i] = vx;
    omx[i] = 1.0 - vx;
    logx[i] = logq(vx.convert_to<__float128>());
    logomx[i] = logq((1.0 - vx).convert_to<__float128>());
    //if (nPoints == 512) cout << vx << " " << omx[i] << " " << logx[i] << " " << logomx[i] << endl;
    //cout << x[i] << " " << logs[i] << " " << logomx[i] << endl;
  }
}

void InDCT::setPrior(double aPrior, double bPrior)
{
  if (debugOut) cout << nPoints << endl;
  int count_0 = 0;
  int count_1 = 0;
/*std::map<myarchive, result> abPrior;
stringstream archivefile;
archivefile << "prior_"<<nPoints<<"_"<<aPrior<<"_"<<bPrior<<".dat";
ifstream ifs(archivefile.str());
filebuf *fb = ifs.rdbuf();
if(fb->is_open()){
        cout << " file exists, reading" << endl;
        boost::archive::text_iarchive iarch(ifs);
        iarch >> abPrior;
}
else
        cout << " File does not exist" << endl;


  mpf_float_100  omvx_hp = 0.0;
  mpf_float_100 vx_hp = 0.0;*/
  for (int i = 0; i < nPoints; i++) {
/*    myarchive abprior(aPrior,bPrior,nPoints,i);
    map<myarchive,result>::iterator it = abPrior.find(abprior);
    if (it != abPrior.end()) {
      //Found it!
	vx_hp = (it->second).vx;
	omvx_hp = (it->second).omvx; 
	cout << " Found " << i << "  " <<vx_hp << "   " << omvx_hp<< endl;
      
      //vx_hp = it->second;
      //cout << it->second;
      //cout << " found it" << endl;
}
else{
      mpf_float_100 xorig_hp = 0.5 * (boost::math::cos_pi<mpf_float_100>((i + 0.5) / nPoints) + 1.0);
      vx_hp = boost::math::ibeta_inv<mpf_float_100>(aPrior, bPrior, xorig_hp, &omvx_hp, policy<digits10<100> >()); //Scaled to unterval from 0 to 1
	      abPrior.insert(std::make_pair(abprior,result(vx_hp,omvx_hp)));
	
      cout << " NO map value found " <<i <<"    " <<  vx_hp<<"   " << omvx_hp << endl;
}
*/
//    bdouble xorig = 0.5 * (boost::math::cos_pi<bdouble>((i + 0.5) / nPoints) + 1.0);
    mpf_float_100 xorig_hp = 0.5 * (boost::math::cos_pi<mpf_float_100>((i + 0.5) / nPoints) + 1.0);
   // bdouble omvx = 0.0;
    mpf_float_100 omvx_hp = 0.0;
   // bdouble vx = boost::math::ibeta_inv<bdouble>(aPrior, bPrior, xorig, &omvx, policy<digits10<100> >()); //Scaled to unterval from 0 to 1
    mpf_float_100 vx_hp = boost::math::ibeta_inv<mpf_float_100>(aPrior, bPrior, xorig_hp, &omvx_hp, policy<digits10<100> >()); //Scaled to unterval from 0 to 1

    if(vx_hp==0.0){
	count_0++;
	isZero[i] = true;
	}
    else{
	isZero[i] = false;
	   if(vx_hp < 0.5)
		logx[i] = -(boost::math::log1p<mpf_float_100>(1/vx_hp - 1)).convert_to<__float128>();
	   else
		logx[i] = (boost::math::log1p<mpf_float_100>(-omvx_hp)).convert_to<__float128>();
       }
   if(omvx_hp==0.0){
	count_1++;
	isOne[i] = 1;
   }
   else{
	isOne[i] = false;
	   if(omvx_hp < 0.5)
		logomx[i] = -(boost::math::log1p<mpf_float_100>(1/omvx_hp - 1)).convert_to<__float128>();
	   else
		logomx[i] = (boost::math::log1p<mpf_float_100>(-vx_hp)).convert_to<__float128>();
   } 
    x[i] = vx_hp.convert_to<float128>();
    omx[i] = omvx_hp.convert_to<float128>();


   // logx[i] = logq(vx.convert_to<__float128>());
   // logomx[i] = logq(omvx.convert_to<__float128>());
   // if (debugOut) cout << (double)xorig << " " << (double)vx << " " << (double)omvx << " " << (double)logx[i] << " " << (double)logomx[i] << endl;
    //cout << x[i] << endl;
  }
 /* try{
  	ofstream ofs(archivefile.str());
  	boost::archive::text_oarchive oarch(ofs);
  	oarch << abPrior;
  } catch(const std::exception &e){
        cout << "caught an exception writing file" << endl;
  }
*/
  cout << "quad precision: number of points zeros " << count_0 << " count ones  "<< count_1 << endl;
  logPriorFactor = logq((boost::math::beta<bdouble>(aPrior, bPrior)).convert_to<__float128>());
  priorSet = true;
  resetTable();
  aPreset = aPrior; bPreset = bPrior;
}

void InDCT::unsetPrior()
{
  for (int i = 0; i < nPoints; i++) {
    //bdouble vx = 0.5 * (boost::math::cos_pi<bdouble>((i + 0.5) / nPoints) + 1.0);
    mpf_float_100 vx_hp = 0.5 * (boost::math::cos_pi<mpf_float_100>((i + 0.5) / nPoints) + 1.0);
    x[i] = vx_hp.convert_to<float128>();
    omx[i] = 1.0 - vx_hp.convert_to<float128>();
    isOne[i] = false;
    isZero[i] = false;
   if(vx_hp < 0.5)
	logx[i] = -(boost::math::log1p<mpf_float_100>(1/vx_hp - 1)).convert_to<__float128>();
   else
	logx[i] = (boost::math::log1p<mpf_float_100>(-(1.0 - vx_hp))).convert_to<__float128>();

   if(vx_hp > 0.5)
	logomx[i] = -(boost::math::log1p<mpf_float_100>(1/(1.0 - vx_hp) - 1.0)).convert_to<__float128>();
   else
	logomx[i] = (boost::math::log1p<mpf_float_100>(-vx_hp)).convert_to<__float128>();

   // logx[i] = logq(vx.convert_to<__float128>());
   // logomx[i] = logq((1.0 - vx).convert_to<__float128>());
//    if (debugOut) cout << (double)vx << " " << (double)omx[i] << " " << (double)logx[i] << " " << (double)logomx[i] << endl;
  }
  logPriorFactor = 0.0;
  aPreset = bPreset = 0.0;
  priorSet = false;
  resetTable();
}

bdouble InDCT::compute(const vector<unsigned> & a, const vector<unsigned>& b) 
{
  vector<double> adbl(a.begin(), a.end());
  vector<double> bdbl(b.begin(), b.end());
  return compute(adbl, bdbl);
}

bdouble InDCT::compute(const vector<double> & a, const vector<double>& b) 
//NOTE: nPoints corresponds to nPoints terms in the expansion. The highest degree is nPoints - 1
{
  if (debugOut) cout << "InDCT::compute() with nPoints = " << nPoints << endl << flush; //assert (false);
  clock_t iniClock = clock();
  numCalls++;
  assert(a.size() == b.size());
  bdouble value = -HUGE_VAL; //Return value
  bool found = false;
  vector<double> ab(a);
  ab.insert(ab.end(), b.begin(), b.end());
  //Try to lookup the value
  if (doLookup) {
    map<vector<double>,bdouble>::iterator it = abToI.find(ab);
    if (it != abToI.end()) {
      //Found it!
      numLookups++;
      value = it->second;
      found = true;
      if (debugOut) cout << "Looked up value = " << (double)value << endl << flush;      
    }  
    lookupClocks += clock() - iniClock;
  } 
  if (!found) {
    if (debugOut) cout << "Was not found, computing..." << endl << flush;
    //Was not looked up or was not found 
    if (!inHouse || priorSet) {
      if (debugOut) cout << "Performing FFTW calculations..." << endl << flush;

      unsigned dim = a.size();
      assert(dim >= 1);
      unsigned startDim = dim - 1;
      if (useIBeta && !priorSet) startDim--;

      for (int d = startDim; d >= 0; d--) {
	//cout << "Dimension " << d << " with parameters " << a[d] << " " << b[d] << endl;
	if (d == startDim) {
	  //Fill f for the first time
	  if (priorSet) {
	    for (int i = 0; i < nPoints; i++) {
			if((isZero[i] &&(a[d] >0))||(isOne[i] &&(b[d] >0)))
				f[i] = 0.0;
			else
			{
			      mydouble sslog = 0.0;
			      if(!isZero[i])
					sslog += a[d]*logx[i];
			     if(!isOne[i])
					sslog += b[d]*logomx[i];

			    //  if((isZero[i]==0)&&(isOne[i]==0))
				
				f[i] = expq(sslog);
			}
	     // if((isZero[i]==0)&&(isOne[i]==0))
		     // f[i] = (boost::math::expm1(a[startDim] * logx[i] + b[startDim] * logomx[i]) + 1.0).convert_to<__float128>();
	/*	      f[i] = expq(a[startDim] * logx[i] + b[startDim] * logomx[i]);
	      else if((isZero[i]==1)&&(isOne[i]==0)){
		if(a[startDim]==0)
			f[i] = 1.0;
		else
			f[i] = 0.0;
	      }
	      else if((isZero[i]==0)&&(isOne[i]==1)){
		if(b[startDim]==0)
			f[i] = 1.0;
		else
			f[i] = 0.0;
	      }
	      else{
		cout << "error in zero/one flags" <<endl << fflush;
		exit(1);
	      } */ 
	    }
	  } else if (useIBeta) {
	    for (int i = 0; i < nPoints; i++) {
	      f[i] = (boost::math::expm1((a[startDim] - 1.0) * logx[i] + (b[startDim] - 1.0) * logomx[i] 
			 + boost::math::log1p(boost::math::beta(a[startDim + 1], b[startDim + 1],x[i]) - 1.0))).convert_to<__float128>() + 1.0;
	    }	  
	  } else {
	    for (int i = 0; i < nPoints; i++) {
	      f[i] = expq((a[startDim] - 1.0) * logx[i] + (b[startDim] - 1.0) * logomx[i]);
	    }
	  }
	} else {
	  //First, perform inverse transform on cInt to get the integrated expansion values on the points in x
	  fftwq_execute ( plan_backward ); //Remeber, the resulting value need to be divided by 2*nPoints
	  if (priorSet) {
	    for (int i = 0; i < nPoints; i++) {
	      if (fNext[i] > 0)
	      {
			if((isZero[i] &&(a[d] >0))||(isOne[i] &&(b[d] >0)))
				f[i] = 0.0;
			else
			{
			      mydouble sslog = logq(fNext[i]);
			      if(!isZero[i])
					sslog += a[d]*logx[i];
			     if(!isOne[i])
					sslog += b[d]*logomx[i];

			    //  if((isZero[i]==0)&&(isOne[i]==0))
				
				f[i] = expq(sslog)/(2*nPoints);
			}
			//f[i] = expq(a[d] * logx[i] + b[d] * logomx[i] + logq(fNext[i])) / (2 * nPoints);
			//f[i] = ((boost::math::expm1(a[d] * logx[i] + b[d] * logomx[i] + logq(fNext[i]))).convert_to<__float128>() +1.0) / (2 * nPoints);
	/*	      else if((isZero[i]==1)&&(isOne[i]==0)){
			if(a[d]==0)
				f[i] = fNext[i] / (2 * nPoints);
			else
				f[i] = 0.0;
		      }
		      else if((isZero[i]==0)&&(isOne[i]==1)){
			if(b[d]==0)
				f[i] = fNext[i]/ (2 * nPoints);
			else
				f[i] = 0.0;
		      }
		      else{
			cout << "error in zero/one flags" <<endl << fflush;
			exit(1);
		      }  */ 
	      }
	      else f[i] = 0.0;
	    }
	  } else {
	    for (int i = 0; i < nPoints; i++) {
	      //f[i] = pow(x[i], a[d] - 1.0) * pow(omx[i], b[d] - 1.0) * fNext[i] / (2 * nPoints);
	      if (fNext[i] > 0)
		f[i] = expq((a[d] - 1.0) * logx[i] + (b[d] - 1.0) * logomx[i] + logq(fNext[i])) / (2 * nPoints);
	      else f[i] = 0.0;
	    }
	  }
	}
	//cout << d << endl;
	//for (int i = 0; i < nPoints; i++) {
	//cout << " " << i << " " << f[i]; 
	//}
	//cout << endl;
	fftwq_execute ( plan_forward ); //This replaces findChebyshev(f, c);
	intChebyshev(nPoints - 1, c, cInt);
	//print();
      }
    
      value = evalChebyshev(1.0, nPoints - 1, cInt) / nPoints; 
      if (priorSet) value *= (boost::math::expm1<bdouble>(dim * logPriorFactor) + 1.0);
    } else {
      assert (inHouse);
      if (debugOut) cout << "Performing inHouse calculations... " << endl << flush;
      value = computeInChebyshev(nPoints, a, b);
    }

    abToI[ab] = value;
    //The factor (1/nPoints) is different because Clenshaw(Integral DCT) = 0.5 * IDCT(Integral DCT)
  }
  totalClocks += clock() - iniClock;
  if (debugOut) cout << "InDCT::compute() finished, value = " << (double)value << endl << flush;
  return value;
}


//void InDCT::computeValueGradHess(const vector<double> & a, const vector<double>& b, 
//			 mydouble &value, vector<mydouble> &grad, vector<vector<mydouble> > &hess) 
/*mydouble InDCT::computeGrad(const vector<double> & a, const vector<double>& b, int k, bool ak)
//NOTE: nPoints corresponds to nPoints terms in the expansion. The highest degree is nPoints - 1
{
  if (debugOut) cout << "InDCT::computeValueGradHess() with nPoints = " << nPoints << endl << flush; //assert (false);
  //clock_t iniClock = clock();
  //numGrads++;
  assert(a.size() == b.size());
  mydouble value = -HUGE_VAL; //Return value

  if (debugOut) cout << "Performing FFTW calculations..." << endl << flush;

  unsigned dim = a.size();
  assert(dim >= 1);
  unsigned startDim = dim - 1;
  if (useIBeta && !priorSet) startDim--;

  //cout << useIBeta << endl;

  for (int d = startDim; d >= 0; d--) {
    //cout << "Dimension " << d << " with parameters " << a[d] << " " << b[d] << endl;
    if (d == startDim) {
      //Fill f for the first time
      if (priorSet) {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[startDim]) * pow(omx[i], b[startDim]);
	}
      } else if (useIBeta) {
	if (k == dim - 1) {
	  assert(k == startDim + 1);
	  double p = a[startDim + 1], q = b[startDim + 1], ppq = p + q, xi = 0;
	  double der[6];
	  int nappx;
	  double errapx;
	  int ifault;
	  double psi[7];
	  psi[0] = lgamma(p) + lgamma(q) - lgamma(ppq);
	  int ier;
	  psi[1] = digama_(&p, &ier);
	  psi[2] = trigam_(&p, &ier);
	  psi[3] = digama_(&q, &ier);
	  psi[4] = trigam_(&q, &ier);
	  psi[5] = digama_(&ppq, &ier);
	  psi[6] = trigam_(&ppq, &ier);
	  //double betaval = boost::math::beta(p, q);
	  for (int i = 0; i < nPoints; i++) {
	    xi = x[i];
	    inbeder_(&xi, &p, &q, psi, der, &nappx, &errapx, &ifault);
	    //double vtest = boost::math::beta(p, q, xi);
	    //if (abs(vtest - der[0]) > 1e-10) cerr << p << " " << q << " " << xi << " " << der[0] << " " << vtest << endl;
	    //cout << der[1] << " " << der[3] << " ";
	    if (ak)
	      f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0) * der[1];
	    else 
	      f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0) * der[3];
	  }	  
	} else {
	  for (int i = 0; i < nPoints; i++) {
	    f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0) * 
	      boost::math::beta(a[startDim + 1], b[startDim + 1], x[i]);
	  } 
	}
      } else {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0);
	}
      }
    } else {
      //First, perform inverse transform on cInt to get the integrated expansion values on the points in x
      fftwq_execute ( plan_backward ); //Remeber, the resulting value need to be divided by 2*nPoints
      if (priorSet) {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[d]) * pow(omx[i], b[d]) * fNext[i] / (2 * nPoints);
	}
      } else {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[d] - 1.0) * pow(omx[i], b[d] - 1.0) * fNext[i] / (2 * nPoints);
	}
      }
    }
    if (d == k) {
      //cout << "d == k == " << d << endl;
      if (ak) {
	for (int i = 0; i < nPoints; i++) {
	  f[i] *= logx[i];
	}
      } else {
	for (int i = 0; i < nPoints; i++) {
	  f[i] *= logomx[i];
	}
      }
    }
    //for (int i = 0; i < nPoints; i++) cout << f[i] << endl;

    fftwq_execute ( plan_forward ); //This replaces findChebyshev(f, c);
    intChebyshev(nPoints - 1, c, cInt);
    //print();
  } 
    
  value = evalChebyshev(1.0, nPoints - 1, cInt) / nPoints; 
  if (priorSet) value *= expq(dim * logPriorFactor);

  //gradClocks += clock() - iniClock;
  if (debugOut) cout << "InDCT::computeGrad() finished, value = " << (double)value << endl << flush;
  return value;
}
*/
/*
mydouble InDCT::computeHess(const std::vector<double> & a, const std::vector<double>& b, int k1, int k2, bool ak1, bool ak2)
{
  if (debugOut) cout << "InDCT::computeValueGradHess() with nPoints = " << nPoints << endl << flush; //assert (false);
  if (k2 < k1) {
    swap(k1, k2);
    swap(ak1, ak2);
  }
  //clock_t iniClock = clock();
  //numGrads++;
  assert(a.size() == b.size());
  mydouble value = -HUGE_VAL; //Return value

  if (debugOut) cout << "Performing FFTW calculations..." << endl << flush;

  unsigned dim = a.size();
  assert(dim >= 1);
  unsigned startDim = dim - 1;
  if (useIBeta && !priorSet) startDim--;

  //cout << useIBeta << endl;

  for (int d = startDim; d >= 0; d--) {
    //cout << "Dimension " << d << " with parameters " << a[d] << " " << b[d] << endl;
    if (d == startDim) {
      //Fill f for the first time
      if (priorSet) {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[startDim]) * pow(omx[i], b[startDim]);
	}
      } else if (useIBeta) {
	if (k2 == dim - 1) {
	  assert(k2 == startDim + 1);
	  double p = a[startDim + 1], q = b[startDim + 1], ppq = p + q, xi = 0;
	  double der[6];
	  int nappx;
	  double errapx;
	  int ifault;
	  double psi[7];
	  psi[0] = lgamma(p) + lgamma(q) - lgamma(ppq);
	  int ier;
	  psi[1] = digama_(&p, &ier);
	  psi[2] = trigam_(&p, &ier);
	  psi[3] = digama_(&q, &ier);
	  psi[4] = trigam_(&q, &ier);
	  psi[5] = digama_(&ppq, &ier);
	  psi[6] = trigam_(&ppq, &ier);
	  //double betaval = boost::math::beta(p, q);
	  for (int i = 0; i < nPoints; i++) {
	    xi = x[i];
	    inbeder_(&xi, &p, &q, psi, der, &nappx, &errapx, &ifault);
	    //double vtest = boost::math::beta(p, q, xi);
	    //if (abs(vtest - der[0]) > 1e-10) cerr << p << " " << q << " " << xi << " " << der[0] << " " << vtest << endl;
	    //cout << der[1] << " " << der[3] << " ";
	    if (ak2)
	      f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0) * der[1];
	    else 
	      f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0) * der[3];
	  }	  
	} else {
	  for (int i = 0; i < nPoints; i++) {
	    f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0) * 
	      boost::math::beta(a[startDim + 1], b[startDim + 1], x[i]);
	  } 
	}
      } else {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[startDim] - 1.0) * pow(omx[i], b[startDim] - 1.0);
	}
      }
    } else {
      //First, perform inverse transform on cInt to get the integrated expansion values on the points in x
      fftwq_execute ( plan_backward ); //Remeber, the resulting value need to be divided by 2*nPoints
      if (priorSet) {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[d]) * pow(omx[i], b[d]) * fNext[i] / (2 * nPoints);
	}
      } else {
	for (int i = 0; i < nPoints; i++) {
	  f[i] = pow(x[i], a[d] - 1.0) * pow(omx[i], b[d] - 1.0) * fNext[i] / (2 * nPoints);
	}
      }
    }
    if (d == k2) {
      //cout << "d == k == " << d << endl;
      if (ak2) {
	for (int i = 0; i < nPoints; i++) {
	  f[i] *= logx[i];
	}
      } else {
	for (int i = 0; i < nPoints; i++) {
	  f[i] *= logomx[i];
	}
      }
    }
    //for (int i = 0; i < nPoints; i++) cout << f[i] << endl;

    fftwq_execute ( plan_forward ); //This replaces findChebyshev(f, c);
    intChebyshev(nPoints - 1, c, cInt);
    //print();
  } 
    
  value = evalChebyshev(1.0, nPoints - 1, cInt) / nPoints; 
  if (priorSet) value *= expq(dim * logPriorFactor);

  //gradClocks += clock() - iniClock;
  if (debugOut) cout << "InDCT::computeHess() finished, value = " << (double)value << endl << flush;
  return value;
}
*/
void InDCT::print()
{
  for (int i = 0; i < nPoints; i++) {
    cout << setw(3) << i << " " 
	 << setw(16) << setprecision(8) << (double)f[i] << " "
	 << setw(16) << setprecision(8) << (double)fNext[i] << " "
	 << setw(16) << setprecision(8) << (double)c[i] << " "
	 << setw(16) << setprecision(8) << (double)cInt[i] << endl;
  }
  cout << endl;
}
/*
mydouble log_binomial_coefficient(unsigned n, unsigned k)
{
  assert(n >= k);
  if (k == 0 || k == n) return 0;
  else if (k == 1 || k == n-1) return logq((mydouble) n);
  else if (2 * k == n) return boost::math::lgamma<mydouble>(n + 1) - 2 * boost::math::lgamma<mydouble>(k + 1);
  else return boost::math::lgamma<mydouble>(n + 1) - boost::math::lgamma<mydouble>(k + 1) - boost::math::lgamma<mydouble>(n - k + 1);
}
*/
struct term {
  mydouble value;
  bool negativeSign;
  term(mydouble v = 0.0, short ns = true) : value(v), negativeSign(ns) {}
};

bool valueLess(const term& t1, const term& t2) { return (t1.value < t2.value); }
/*
mydouble computeInInteger(unsigned n, const vector<unsigned> & a, const vector<unsigned>& b) 
{
  assert(n > 0);
  //cerr << a.size() << " " << b.size() << endl << flush;
  assert(b.size() == n && a.size() == n);
  unsigned an = a.back(), bn = b.back();
  assert(bn >= 1 && an >= 1);
  if (n == 1) return boost::math::beta<mydouble>(an, bn);
  else {
    unsigned a1 = a.front(), b1 = b.front();
    vector<term> logTerms;
    mydouble result = 0;
    if (bn <= a1) {
      //Shrink from the back (b_n cases; integrate out theta_n)
      //Allocate shrunk parameter vector
      vector<unsigned> awon(a.begin(), a.end()-1), bwon(b.begin(), b.end()-1);
      awon.back() += an;
      for (int i = 0; i < bn; i++) {
	logTerms.push_back(term(log_binomial_coefficient(bn - 1, i) + 
				logq(computeInInteger(n-1, awon, bwon)) - 
				logq((mydouble) an + i), (i % 2 == 1)));
	awon.back() += 1;
      }
    } else {
      //Shrink from the front (a_n cases; integrate out theta_1)
      //Allocate shrunk parameter vector
      vector<unsigned> awo1(a.begin()+1, a.end()), bwo1(b.begin()+1, b.end());
      bwo1.front() += b1;
      for (int i = 0; i < a1; i++) {
	logTerms.push_back(term(log_binomial_coefficient(a1 - 1, i) + 
				logq(computeInInteger(n-1, awo1, bwo1)) - 
				logq((mydouble) b1 + i), (i % 2 == 1)));
	bwo1.front() += 1;
      }
    }
    sort(logTerms.begin(), logTerms.end(), valueLess);
    for (vector<term>::const_iterator it = logTerms.begin(); it != logTerms.end(); it++) {
      //cout << it->value << " " ;
      if (it->negativeSign) result -= expq(it->value);
      else result += expq(it->value);
    }
    //cout << ":" << setprecision(10) << result << endl;
    return result;
  }
}
*/
/*
mydouble directComputeLNI(const vector<double>& a)
{
  mydouble suma = 0;
  mydouble sumlogsuma = 0;
  for (vector<double>::const_reverse_iterator it = a.rbegin(); it != a.rend(); ++it) {
    suma += *it;
    sumlogsuma += logq(suma);
  }
  return -sumlogsuma;
}
*/
/*
mydouble directComputeLNIRatio(const vector<double>& aref, const vector<double>& a)
{
  assert(aref.size() == a.size());
  mydouble suma = 0;
  mydouble sumaref = 0;
  mydouble sumlogratio = 0;
  for (int i = a.size() - 1; i >= 0; i--) {
    suma += a[i];
    sumaref += aref[i];
    sumlogratio += logq(sumaref) - logq(suma);
  }
  return sumlogratio;
}
*/
/*
mydouble directComputeIRatio(mydouble logNormConst, const vector<double>& a, const vector<idxn> & in)
{
  if (in.size() == 0) return expq(logNormConst + directComputeLNI(a));

  vector<double> aNew = a;
  idxn inLast = in.back();
  vector<idxn> inNew = in; 
  inNew.pop_back();
  mydouble result = 0;
  for (int l = inLast.n; l >= 0; l--) {
    aNew[inLast.idx] += l;
    mydouble value = boost::math::binomial_coefficient<mydouble>(inLast.n, l) 
      * directComputeIRatio(logNormConst, aNew, inNew);
    if (l % 2 == 1) result -= value;
    else result += value;
    aNew[inLast.idx] -= l;    
  }
  return result;
}
*/
/*
mpq_rational directComputeIinRational(const vector<mpq_rational>& a)
{
  mpq_rational suma = 0;
  mpq_rational prodsuma = 1;
  for (vector<mpq_rational>::const_reverse_iterator it = a.rbegin(); it != a.rend(); ++it) {
    suma += *it;
    prodsuma *= suma;
    //cout << suma << " " << prodsuma << endl << flush;
  }
  assert(prodsuma > 0);
  return mpq_rational(1)/prodsuma;
}

mpq_rational directComputeIinRational(const vector<mpq_rational>& a, const vector<idxn> & in)
{
  if (in.size() == 0) {
    return directComputeIinRational(a);
  }

  mpq_rational result = 0;
  vector<mpq_rational> aNew = a;
  idxn inLast = in.back();
  vector<idxn> inNew = in; 
  inNew.pop_back();
  for (int l = inLast.n; l >= 0; l--) {
    aNew[inLast.idx] += l;
    //For small values of n, it should be possible to just round
    mpq_rational binvalue = static_cast<mpq_rational>(boost::math::binomial_coefficient<double>(inLast.n, l));
    mpq_rational value = binvalue * directComputeIinRational(aNew, inNew);
    if (l % 2 == 1) result -= value;
    else result += value;
    aNew[inLast.idx] -= l;    
  }
  return result;
}
*/
