#ifndef SET_TRIPLE
#define SET_TRIPLE

#include <cassert>
#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>

inline double expU(double x, double r)
{
  return 1 - exp(-x/r);
}

inline double CEexpU(double u, double r)
{
  return -r * log(1 - u);
}

/*
  inline double expU(double x, double r)
  {
  return r * (1 - exp(-x/r));
  }

  inline double CEexpU(double u, double r)
  {
  return -r * log(1 - u / r);// r * (1 - exp(-x/r));
  }
*/

//QnD bit set functions
unsigned short card(unsigned long s);

inline unsigned long setBit(unsigned long s, unsigned short bit) 
{
  return s | (1 << bit);
}

inline unsigned long unsetBit(unsigned long s, unsigned short bit) 
{
  return s & ~(1 << bit);
}

inline bool isSet(unsigned long s, unsigned short bit) {
  return (s & (1 << bit)) != 0;
}

class SetTriple {
  unsigned short N, f, s1, s2;
  unsigned long data;

  void resetCounters();

public:
  unsigned short getN() const { return N; }
  unsigned short getF() const { return f; }
  unsigned short getS1() const { return s1; }
  unsigned short getS2() const { return s2; }
  unsigned short getStatus(unsigned short i) const
  {
    assert(i < N);
    return (data >> (2*i)) & 3;
  }
  void setStatus(unsigned short i, unsigned short status);
  double findSurplus(const std::vector<double>& v, double p1, double p2, double p3) const;
  double findUtilities(std::vector<double>& u, const std::vector<double>& v, const std::vector<double>& rho, 
		       double p1, double p2, double p3) const;
  double findUtilities(std::vector<double>& u, 
		       const std::vector<double>& w1,
		       const std::vector<double>& w2, 
		       const std::vector<double>& w3) const; 
  void print(std::ostream& ostr, bool formatted = true) const;
  void next();
  void getRemaining(std::vector<unsigned short>& r) const;
  long getNS() const;
  void setToNS(long ns);

  SetTriple(unsigned short Nini) : N(Nini), f(Nini), s1(0), s2(0), data(0l) 
  {
    assert(Nini <= 4 * sizeof(unsigned long));
  }
  SetTriple(unsigned short Nini, unsigned long dataIni) : N(Nini), s1(0), s2(0), data(dataIni) 
  {
    assert(N <= 4 * sizeof(unsigned long));
    assert((data >> (2*N)) == 0);
    resetCounters();
  }
  SetTriple(const SetTriple& st) : N(st.N), f(st.f), s1(st.s1), s2(st.s2), data(st.data) {}

};

std::ostream& operator<<(std::ostream& ostr, const SetTriple& st);


#endif
