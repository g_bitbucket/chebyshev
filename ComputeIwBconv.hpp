#ifndef COMPUTEIWBCONV
#define COMPUTEIWBCONV

#include <iostream>
#include <vector>
#include <array>
#include <math.h>
#include <time.h>
#define VERBOSE	0
#include "ComputeIwB.hpp"
using namespace std;

template<class T>
void convsse(T *input1,T *input2,int size, T *res){
  for(int i = 0; i < 2*size - 1; i++)
    for(int j =0 ; j < 2*size - 1; j++)
      if((i - j >= 0) && ((i -j) < size) && (j <size))
	res[i] += input1[j]*input2[i-j];

}
template<class T>
void compute_new_coeff_conv(T *c,T a,T b,int size){
  T w[size];
  T res[2*size -1];
  T cc[size];
  fill_n(res,2*size -1,0);
  w[0] = 1;
  for(int i =1; i <size; i++)
    {
      cc[i-1] = c[i];
      w[i] = w[i-1] * (T)(i - b)/(T)i;
    }
  cc[size -1] = c[size];
  /*
    if(VERBOSE >=10){
    cout << "w:";
    for(int i = 0 ; i < size;i++)
    cout << w[i] << " ";
    cout << endl;
    cout << "cc:";
    for(int i = 0 ; i < size;i++)
    cout << cc[i] << " ";
    cout << endl;
    }
  */
  convsse<T>(w,cc,size,res);

  //cout << "conv:";
  //for(int i = 0 ; i < 2*size - 1;i++){
  //       if(VERBOSE>=10) cout << res[i] << " ";
  //      }
  //        cout << endl;
  c[0] += a;
  for(int i = 1; i <= size; i++){
    c[i] = res[i-1]/(i-1 + c[0]);
  }
}
/*
//template<class T>
//long double compute_sum(T *c, int length){
//long double res = 0;
//for(int i = 0; i < length; i++)
//	res += (c[i+1].template convert_to<double>())*powl(0.5,i+c[0]);
//return res;
//}
*/
template <class T>
void inverseArray_conv(T *a,T *aInv, unsigned size){
  for(int i = 0; i < size; i++)
    aInv[i] = a[size - i  - 1];
}
/*
  template <class T>
  long double  computeIwBconv(boost::multiprecision::mpq_rational *a,boost::multiprecision::mpq_rational *b,unsigned size){
  long double XX = 0;
  //  int sec =0 ;
  // double long nsec = 0; 
  //  struct timespec tt;
  T X = 0;
  T A[size+1],B[size+1];
  T c[N+1],cInv[N+1];
  T aInv[size],bInv[size];
  inverseArray<T>(a,aInv,size);
  inverseArray<T>(b,bInv,size);
  std::fill_n(c,N+1,0);
  std::fill_n(cInv,N+1,0);
  std::fill_n(A,size+1,0);
  std::fill_n(B,size+1,0);
  A[0] = B[0] = 1;
  c[1] = cInv[1] = 1;
  //  clock_gettime(CLOCK_REALTIME,&tt);
  //   sec = tt.tv_sec;
  //  nsec = tt.tv_nsec;
  for(int i = 0; i < size ; i++){
  compute_new_coeff_conv<T>(c,a[i],b[i],N);
  cout << "dimension "<< i << endl;
  if(VERBOSE >= 10){
  for(int j = 0; j < N+1; j++)
  cout << c[j] << ",";
  cout << endl;
  }
  A[i+1] = compute_sum<T>(c,N);
  compute_new_coeff_conv<T>(cInv,bInv[i],aInv[i],N);
  B[i+1] = compute_sum<T>(cInv,N);
  }
  for(int i = 0; i< size+1;i++)
  X += A[i]*B[size-i];
  // clock_gettime(CLOCK_REALTIME,&tt);
  // sec = tt.tv_sec -sec;
  //  nsec= tt.tv_nsec - nsec;
  //   nsec = nsec * 1e-9;
  // cout << " time in nanosecs " << nsec << " adjusted " << 1 + nsec << " --> in secs " <<  endl;
  //    if(nsec < 0) {
  //	if (sec > 0 ) sec--;
  //	nsec = 1 + nsec;
  //  }
  //   cout << "rational " << sec<<"," << nsec << " (seconds)"<< endl;
  XX = X.template convert_to<double>();
  return XX;

  }
*/
template <class T>
T computeIwBconv(T *a,T *b,unsigned size){
  int sec =0 ;
  double long nsec = 0; 
  struct timespec tt;
  T X;
  T A[size+1],B[size+1];
  T c[N+1],cInv[N+1];
  T aInv[size],bInv[size];
  inverseArray<T>(a,aInv,size);
  inverseArray<T>(b,bInv,size);
  std::fill_n(c,N+1,0);
  std::fill_n(cInv,N+1,0);
  std::fill_n(A,size+1,0);
  std::fill_n(B,size+1,0);
  A[0] = B[0] = 1;
  c[1] = cInv[1] = 1;
  X =0;
  //  clock_gettime(CLOCK_REALTIME,&tt);
  // sec = tt.tv_sec;
  //  nsec = tt.tv_nsec;
  for(int i = 0; i < size; i++)
    cout << a[i]<< " " ;
  cout << endl;
  for(int i = 0; i < size; i++)
    cout << b[i]<< " " ;
  cout << endl;
  for(int i = 0; i < size ; i++){
    //cout << "dimension "<< i << endl;
    compute_new_coeff_conv<T>(c,a[i],b[i],N);
    if(VERBOSE >= 10){
      for(int j = 0; j < N+1; j++)
	cout << c[j] << ",";
      cout << endl;
    }
    A[i+1] = compute_sum<T>(c,N);
    compute_new_coeff_conv<T>(cInv,bInv[i],aInv[i],N);
    B[i+1] = compute_sum<T>(cInv,N);
  }
  for(int i = 0; i< size+1;i++)
    X += A[i]*B[size-i];
  //  clock_gettime(CLOCK_REALTIME,&tt);
  //  sec = tt.tv_sec -sec;
  //  nsec= tt.tv_nsec - nsec;
  //  nsec = 1e-9 * nsec;
  //cout << " time in nanosecs " << nsec << " adjusted " << 1 + nsec << "--> in secs " << endl;
  //  if(nsec < 0 ) {
  //	if(sec > 0) sec--;
  //	nsec = 1e+9 + nsec;
  // }
  //    cout << "ldouble " << sec<<"." << nsec << "(seconds)" << endl;
  //   cout <<"Normal Conv Value is : " << X <<"  Took "<<((float)t)/CLOCKS_PER_SEC << " clicks" <<  endl;
  return X;

}
#endif
