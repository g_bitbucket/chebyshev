#ifndef DIRECTICOMPUTE
#define DIRECTICOMPUTE

#include <vector>
#include <cmath>
#include <iostream>
#include <boost/math/special_functions/binomial.hpp>
#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/type_traits.hpp>
//using namespace boost::multiprecision;
//#include <boost/numeric/ublas/vector_sparse.hpp>
//#include <boost/numeric/ublas/vector.hpp>
//#include <boost/numeric/ublas/io.hpp>
//using namespace boost::numeric;

struct idxn {
  unsigned idx, n;
  idxn(unsigned idxIni, unsigned nIni) : idx(idxIni), n(nIni) {}
};

inline std::ostream& operator<<(std::ostream& ostr, const std::vector<idxn>& v)
{
  for (std::vector<idxn>::const_iterator it = v.begin(); it != v.end(); ++it)
    ostr << " " << it->idx << " " << it->n;
  return ostr;
}

template<class T>
inline int round_to_int(T v) 
{
  if (boost::is_integral<T>::value) 
    return static_cast<int>(v);
  else if (boost::is_floating_point<T>::value) 
    return static_cast<int>(boost::math::lround(v));
}

/*
  #include <boost/multiprecision/mpfr.hpp>
  #include <boost/multiprecision/number.hpp>
  #include <vector>

  using boost::multiprecision::mpq_rational;

  template <typename rational>
  inline int round_to_int(rational v)
  {
  v = v + typename rational::number(v.sign()) / 2;
  return v.template convert_to<int>();
  }*/

template<>
inline int round_to_int(boost::multiprecision::mpq_rational v)
{
  boost::multiprecision::mpz_int vf_mpz = boost::multiprecision::numerator(v) / boost::multiprecision::denominator(v);
  mpz_t z;
  mpz_init(z);
  mpz_set(z, vf_mpz.backend().data());
  int vf = mpz_get_si(z);
  if (v - vf_mpz < vf_mpz + 1 - v) return vf;
  else return vf + 1;
}

template <class T>
void vectorToIdxn(const std::vector<T>& v, std::vector<idxn>& in)
{
  in.clear();
  for (int i = 0; i < v.size(); i++) {
    //    int vi = (int)boost::multiprecision::round(static_cast<mpf_float>(v[i]));
    int vi = round_to_int(v[i]);
    if (vi < 0) vi = 0;
    if (vi > 0) in.push_back(idxn(i,(unsigned) vi));
    //cout << " " << i << ":" << v[i] << ":" << vi;
  }
  //cout << in << endl;
}

template <class T>
T directComputeI(const std::vector<T>& a, const T& factor = T(1))
{
  T suma = 0;
  T prodsuma = 1;
  for (typename std::vector<T>::const_reverse_iterator it = a.rbegin(); it != a.rend(); ++it) {
    suma += *it;
    prodsuma *= suma;
    //cout << suma << " " << prodsuma << endl << flush;
  }
  assert(prodsuma > 0);
  return factor/prodsuma;
}

template <class T>
T my_binomial_coefficient(int n,int k){

  T tmpval = 1;
  if(k>= n/2){
    for(int i = 0; i < n-k ; i++){
      tmpval = tmpval * (n - i) / (n - k - i);
    }
  } else{
    for(int i = 0; i < k ; i++){
      tmpval = tmpval * (n - i) / (k - i);
    }
    return tmpval;
  }
}
template <class T>
T my_binomial_coefficient(unsigned n,int k){

  T tmpval = 1;
  if(k>= n/2){
    for(int i = 0; i < n-k ; i++){
      tmpval = tmpval * (n - i) / (n - k - i);
    }
  } else{
    for(int i = 0; i < k ; i++){
      tmpval = tmpval * (n - i) / (k - i);
    }
    return tmpval;
  }
}
template <class T>
T directComputeI(const std::vector<T>& a, const std::vector<idxn> & in, const T& factor = T(1))
{
  if (in.size() == 0) {
    return directComputeI(a, factor);
  }

  T result = 0;
  std::vector<T> aNew = a;
  idxn inLast = in.back();
  std::vector<idxn> inNew = in; 
  inNew.pop_back();
  for (int l = inLast.n; l >= 0; l--) {
    aNew[inLast.idx] += l;
    //For small values of n, it should be possible to just round
    //cout << inLast.n << " " << l << endl << flush;
    //T binvalue = static_cast<T>(boost::math::binomial_coefficient<boost::multiprecision::cpp_dec_float>(inLast.n, l));
    T binvalue = my_binomial_coefficient<T>(inLast.n, l);
    T value = binvalue * directComputeI(aNew, inNew);
    if (l % 2 == 1) result -= value;
    else result += value;
    aNew[inLast.idx] -= l;    
  }
  return result;
}

#endif
