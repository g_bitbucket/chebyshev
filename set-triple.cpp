#include "set-triple.hpp"

using namespace std;

ostream& operator<<(ostream& ostr, const SetTriple& st)
{
  for (int i = 0; i < st.getN(); i++) {
    ostr << st.getStatus(i);
  }
  return ostr;
}

void SetTriple::resetCounters() 
{
  s1 = s2 = 0;
  for (unsigned short i = 0; i < N; i++) {
    unsigned short status = getStatus(i);
    assert(status <= 2);
    switch (status) {
    case 1: s1++; break;
    case 2: s2++; break;
    }
  }
  f = N - s1 - s2;
}

double SetTriple::findSurplus(const vector<double>& v, double p1, double p2, double p3) const
{
  assert(v.size() == N);
  double surplus = 0;
  for (int i = 0; i < N; i++) {
    unsigned short status = getStatus(i);
    assert(status <= 2);
    switch (status) {
    case 1: surplus += max(0.0, v[i] - p1); break;
    case 2: surplus += max(0.0, v[i] - p2); break;
    default: surplus += max(0.0, v[i] - p3);
    }
  }
  //cout << endl << surplus << endl;
  return surplus;
}

double SetTriple::findUtilities(std::vector<double>& u, 
				const std::vector<double>& w1,
				const std::vector<double>& w2, 
				const std::vector<double>& w3) const
{
  double total = 0.0;
  assert(w1.size() == N);
  assert(w2.size() == N);
  assert(w3.size() == N);
  u.resize(N);
  for (int i = 0; i < N; i++) {
    unsigned short status = getStatus(i);
    assert(status <= 2);
    switch (status) {
    case 1: u[i] = w1[i]; break;
    case 2: u[i] = w2[i]; break;
    default: u[i] = w3[i];
    }
    total += u[i];
  }
  return total;
}

double SetTriple::findUtilities(vector<double>& u, const vector<double>& v, const vector<double>& rho, double p1, double p2, double p3) const
{
  double total = 0.0;
  assert(v.size() == N);
  assert(rho.size() == N);
  u.resize(N);
  for (int i = 0; i < N; i++) {
    unsigned short status = getStatus(i);
    assert(status <= 2);
    switch (status) {
    case 1: u[i] = expU(max(0.0, v[i] - p1), rho[i]); break;
    case 2: u[i] = expU(max(0.0, v[i] - p2), rho[i]); break;
    default: u[i] = expU(max(0.0, v[i] - p3), rho[i]);
    }
    total += u[i];
  }
  return total;
}

void SetTriple::setStatus(unsigned short i, unsigned short status)
{
  assert(i < N);
  assert(status <= 2);
  unsigned short oldStatus = (data >> (2*i)) & 3;
  if (status != oldStatus) {
    data = (data & ~(3 << (2*i))) | status << (2*i);
    switch (oldStatus) {
    case 1: s1--; break;
    case 2: s2--; break;
    }
    switch (status) {
    case 1: s1++; break;
    case 2: s2++; break;
    }
    f = N - s1 - s2;
  }
}  

void SetTriple::print(std::ostream& ostr, bool formatted) const
{
  if (formatted) {
    for (unsigned short i = 0; i < N; i++) 
      ostr << setw(3) << i << setw(3) << getStatus(i) << endl;
  } else 
    ostr << data;
}

void SetTriple::next()
{
  unsigned short i = 0;
  do {
    unsigned short status = getStatus(i) + 1;
    if (status < 3) {
      setStatus(i, status);
      break;
    } else {
      setStatus(i, 0);
      i++;
    }
  } while (i < N);
  resetCounters();
}

void SetTriple::getRemaining(vector<unsigned short>& r) const
{
  r.resize(f);
  if (f > 0) {
    int pos = 0;
    for (unsigned short i = 0; i < N; i++) {
      if (getStatus(i) == 0) r[pos++] = i;
    }
    assert(pos == f);
  }
}

long SetTriple::getNS() const
{
  long ns = 0;
  for (unsigned short i = 0; i < N; i++) {
    ns += getStatus(i) * long(pow(3.0,i));
  }
  return ns;
}

void SetTriple::setToNS(long ns)
{
  assert(ns >= 0 && ns < long(pow(3.0, N)));
  for (unsigned short i = 0; i < N; i++) {
    setStatus(i,(unsigned short)ns % 3);
    ns /= 3;
  }
}

unsigned short card(unsigned long s)
{
  unsigned short c = 0;
  for (unsigned long d = s; d != 0; d >>= 1) c += d & 1;
  return c;
}

