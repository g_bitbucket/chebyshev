#ifndef CHEBYSHEV_Q
#define CHEBYSHEV_Q

#include <fftw3.h>

#include <iostream>
#include <sstream>
#include <fstream>
#include <utility>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <vector>
#include <map>
#include <algorithm>
#include <cassert>
#include <quadmath.h>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/special_functions/cos_pi.hpp>
#include <boost/math/special_functions/binomial.hpp>
#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/float128.hpp> 
#include <boost/math/constants/constants.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

#include "directicompute.hpp"
using namespace std;
using namespace boost::multiprecision;

namespace boost { namespace serialization {

    template <typename Archive>
    void save(Archive& ar, ::boost::multiprecision::backends::gmp_float<100> const& r, unsigned )
    {
      std::string tmp = r.str(10000, std::ios::fixed) ;
      ar & tmp;
    }

    template <typename Archive>
    void load(Archive& ar, ::boost::multiprecision::backends::gmp_float<100>& r, unsigned )
    {
      std::string tmp;
      ar & tmp;
      r = tmp.c_str();
    }

  } }


BOOST_SERIALIZATION_SPLIT_FREE(::boost::multiprecision::backends::gmp_float<100>)
/*
  class myarchive
  {
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive & ar, const unsigned int version){
  ar & aPrior;
  ar & bPrior;
  ar & nPoints;
  ar & index;
  }       
  public:
  double aPrior,bPrior;
  int nPoints,index;
  myarchive(double a,double b,int npoints,int ind):aPrior(a),bPrior(b),nPoints(npoints),index(ind){};
  myarchive():aPrior(),bPrior(),nPoints(),index(){};
        
  };      
  bool operator<(const myarchive& v1, const myarchive& v2)
  {
  return (v1.aPrior< v2.bPrior)||
  ((v1.aPrior==v2.bPrior)&&(v1.nPoints<v2.nPoints))||
  ((v1.aPrior==v2.bPrior)&&(v1.nPoints==v2.nPoints)&&(v1.index < v2.index));
  }
  bool operator==(const myarchive& v1, const myarchive& v2)
  {
  return ((v1.aPrior==v2.bPrior)&&(v1.nPoints==v2.nPoints)&&(v1.index==v2.index));
  }
*/       
typedef __float128 mydouble;
typedef boost::multiprecision::float128 bdouble;

const long double pi = 4.0 * atan((long double) 1.0);
mydouble directComputeLNI(const std::vector<double>& a);
mydouble directComputeIRatio(mydouble logNormConst, const std::vector<double>& a, const std::vector<idxn> & in);

boost::multiprecision::mpq_rational directComputeIinRational(const std::vector<boost::multiprecision::mpq_rational>& a);
boost::multiprecision::mpq_rational directComputeIinRational(const std::vector<boost::multiprecision::mpq_rational>& a, const std::vector<idxn> & in);


//Find Chebyshev expansion
void findChebyshev(const std::vector<bdouble>& f, std::vector<bdouble>& c);
//Evaluate Chebyshev expansion by Clenshaw's method
bdouble evalChebyshev(bdouble x, const std::vector<bdouble>& c);
bdouble evalChebyshev(bdouble x, unsigned n, const bdouble* c);
mydouble evalChebyshev(mydouble x, const std::vector<mydouble>& c);
mydouble evalChebyshev(mydouble x, unsigned n, const mydouble* c);
//Integrate Chebyshev expansion
void intChebyshev(const std::vector<bdouble>& c, std::vector<bdouble>& cInt);
void intChebyshev(int n, const bdouble* c, bdouble* cInt);
void intChebyshev(const std::vector<mydouble>& c, std::vector<mydouble>& cInt);
void intChebyshev(int n, const mydouble* c, mydouble* cInt);
//Evaluate log-likelihood at roots
void evalLLTermAtRoots(unsigned n, double a, double b, std::vector<bdouble>& f);
void evalLLTermTimesChebyshevAtRoots(unsigned n, double a, double b, const std::vector<bdouble>& c, std::vector<bdouble>& f);

extern "C" {
  //void inbeder_(const double *px, const double *pp, const double *pq, const double *ppsi, 
  //			   double* pder, int* pnappx, double* perrapx, int* ifault);
  void inbeder_(const double *px, const double *pp, const double *pq, const double *ppsi,
		double* pder, int* pnappx, double* perrapx, int* ifault);

  double digama_(const double *px, int* pifault);
  double trigam_(const double *px, int* pifault);
}

//Compute I_n using Chebyshev expansion: works for any numeric type T
template <class T>
bdouble computeInChebyshev(unsigned n, const std::vector<T> & a, const std::vector<T>& b)
{
  assert(a.size() == b.size());
  unsigned dim = a.size();
  assert(dim >= 1);
  std::vector<bdouble> f, c, cInt;
  
  for (int d = dim - 1; d >= 0; d--) {
    if (d == dim - 1) evalLLTermAtRoots(n, a[dim - 1], b[dim - 1], f);
    else evalLLTermTimesChebyshevAtRoots(n, a[d], b[d], cInt, f);
    findChebyshev(f, c);
    intChebyshev(c, cInt);
  }

  return evalChebyshev(1.0, cInt);
}

/*struct idxab {
  unsigned idx;
  double a, b;
  };*/

//A class for computing I_n using DCT
class InDCT {
  unsigned nPoints;
  unsigned long numCalls, numLookups;
  clock_t totalClocks, lookupClocks;
  bool doLookup;
  std::map<std::vector<double>, bdouble> abToI,abPrior;
  bdouble *x;
  bdouble *omx;
  mydouble *logx;
  mydouble *logomx;
  mydouble *f;
  mydouble *fNext;
  mydouble *c;
  mydouble *cInt;
  bool	*isZero;
  bool  *isOne;
  fftwq_plan plan_backward;
  fftwq_plan plan_forward;
  mydouble logPriorFactor;
  bool priorSet, useIBeta, inHouse, debugOut;
  double aPreset, bPreset;
public:
  double getAPreset() const { return aPreset; }
  double getBPreset() const { return bPreset; }
  InDCT(unsigned nP, bool lookupTable = false);
  ~InDCT()
  {
    fftwq_destroy_plan ( plan_forward );
    fftwq_destroy_plan ( plan_backward );

    delete[] x;
    delete[] logx;
    delete[] logomx;
    fftwq_free (f);
    fftwq_free (fNext);
    fftwq_free (c);
    fftwq_free (cInt);
  }
  //Prior is restricted to have the same parameter for each dimension
  void setPrior(double aPrior, double bPrior); //NOTE: resets table
  void unsetPrior(); //NOTE: resets table
  void setLastDimMethod(bool useIBetaFlag) { useIBeta = useIBetaFlag; }
  bdouble compute(const std::vector<double>& a, const std::vector<double>& b);
  bdouble compute(const std::vector<unsigned>& a, const std::vector<unsigned>& b);
  mydouble computeGrad(const std::vector<double> & a, const std::vector<double>& b, int k, bool ak);
  mydouble computeHess(const std::vector<double> & a, const std::vector<double>& b, int k1, int k2, bool ak1, bool ak2);
  void print();
  void resetTiming() 
  {
    numCalls = numLookups = 0; totalClocks = lookupClocks = 0;
  }
  unsigned long getNumCalls() const { return numCalls; }
  double getExecutionTime() const { return (double) totalClocks / CLOCKS_PER_SEC; }
  unsigned long getNumLookups() const { return numLookups; } //Successful lookups only
  double getLookupTime() const { return (double) lookupClocks / CLOCKS_PER_SEC; } //Time spend on all lookups 
  void resetTable() { abToI.clear(); }
  void setInHouse(bool flag) { inHouse = flag; }
  void setDebugOut(bool flag) { debugOut = flag; }
};

mydouble log_binomial_coefficient(unsigned n, unsigned k);
mydouble computeInInteger(unsigned n, const std::vector<unsigned> & a, const std::vector<unsigned>& b);

//Declare standard evaluators of In
extern InDCT calcIn8;
extern InDCT calcIn128;
extern InDCT calcIn256;
extern InDCT calcIn512;
extern InDCT calcIn1024;
extern InDCT calcIn2048;
extern InDCT calcIn4096;
extern InDCT calcIn8192;
extern InDCT calcIn16384;
extern InDCT calcIn65536;

#endif
