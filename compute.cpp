#include <iostream>
#include "ComputeIwB.hpp"
#include "ComputeIwBconv.hpp"
#include <time.h>
#include <gmp.h>
#include <boost/multiprecision/gmp.hpp>
#include <boost/multiprecision/rational_adaptor.hpp>
#include <quadmath.h>
#include "directicompute.hpp"
#include "chebyshev_q.hpp"
using namespace std;
using namespace boost::multiprecision;

mpq_rational computeIwBconv_rational(mpq_rational *a,mpq_rational *b,unsigned size);
mpf_float_100 computeIwBconv_mpf_float_100(mpf_float_100 *a,mpf_float_100 *b,unsigned size);
long double computeIwBconvfft(long double *a,long double *b,unsigned size);

void testavg(){
  unsigned n = 5;
  unsigned nn = 10;
  const int  nPrices = 10;
  boost::multiprecision::mpf_float_100 X11;
  boost::multiprecision::mpf_float_100 step_mpf = 6.0;
  long double stepd = 6;
  boost::multiprecision::mpf_float_100 a_mpf[nPrices] = {0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01};
  boost::multiprecision::mpf_float_100 b_mpf[nPrices] = {0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05,0.05};
  double aa = 0.01;
  double bb = 0.05;
  double priora = 0.01;
  double priorb = 0.05;
  vector<double> adbl(nPrices,aa-priora ),bdbl(nPrices,bb-priorb );
  calcIn16384.setPrior(priora,priorb);
  boost::multiprecision::float128 elem1 = calcIn16384.compute(adbl,bdbl);
  X11 = computeIwBconv_mpf_float_100(a_mpf,b_mpf,nn);
  cout << "value is mp_real " <<X11 << "   " << elem1 << endl;

  for(int i1 = 0; i1 <n ; i1++){ 
    for(int i2 = 0; i2 <n ; i2++){ 
      for(int i3 = 0; i3 <n ; i3++){ 
	for(int i4 = 0; i4 <n ; i4++){ 
	  for(int i5 = 0; i5 <n ; i5++){ 
	    for(int i6 = 0; i6 <n ; i6++){ 
	      for(int i7 = 0; i7 <n ; i7++){ 
		for(int i8 = 0; i8 <n ; i8++){ 
		  for(int i9 = 0; i9 <n ; i9++){ 
		    for(int i10 = 0; i10 <n ; i10++){ 
		      for(int j1 = 0; j1 <n ; j1++){ 
			for(int j2 = 0; j2 <n ; j2++){ 
			  for(int j3 = 0; j3 <n ; j3++){ 
			    for(int j4 = 0; j4 <n ; j4++){ 
			      for(int j5 = 0; j5 <n ; j5++){ 
				for(int j6 = 0; j6 <n ; j6++){ 
				  for(int j7 = 0; j7 <n ; j7++){ 
				    for(int j8 = 0; j8 <n ; j8++){ 
				      for(int j9 = 0; j9 <n ; j9++){ 
					for(int j10 = 0; j10 <n ; j10++){ 
					  mpf_float_100 a_mpf_t[nPrices],b_mpf_t[nPrices];
					  vector<double> adbl_t(nPrices),bdbl_t(nPrices);
					  a_mpf_t[0] = a_mpf[0] + i1;  b_mpf_t[0]=b_mpf[0]+j1;
					  a_mpf_t[1] = a_mpf[1] + i2;  b_mpf_t[1]=b_mpf[1]+j2;
					  a_mpf_t[2] = a_mpf[2] + i3;  b_mpf_t[2]=b_mpf[2]+j3;
					  a_mpf_t[3] = a_mpf[3] + i4;  b_mpf_t[3]=b_mpf[3]+j4;
					  a_mpf_t[4] = a_mpf[4] + i5;  b_mpf_t[4]=b_mpf[4]+j5;
					  a_mpf_t[5] = a_mpf[5] + i6;  b_mpf_t[5]=b_mpf[5]+j6;
					  a_mpf_t[6] = a_mpf[6] + i7;  b_mpf_t[6]=b_mpf[6]+j7;
					  a_mpf_t[7] = a_mpf[7] + i8;  b_mpf_t[7]=b_mpf[7]+j8;
					  a_mpf_t[8] = a_mpf[8] + i9;  b_mpf_t[8]=b_mpf[8]+j9;
					  a_mpf_t[9] = a_mpf[9] + i10; b_mpf_t[9]=b_mpf[9]+j10;
					  adbl_t[0] = adbl[0] + i10; bdbl_t[0]=bdbl[0]+j10;
					  adbl_t[1] = adbl[1] + i9;  bdbl_t[1]=bdbl[1]+j9;
					  adbl_t[2] = adbl[2] + i8;  bdbl_t[2]=bdbl[2]+j8;
					  adbl_t[3] = adbl[3] + i7;  bdbl_t[3]=bdbl[3]+j7;
					  adbl_t[4] = adbl[4] + i6;  bdbl_t[4]=bdbl[4]+j6;
					  adbl_t[5] = adbl[5] + i5;  bdbl_t[5]=bdbl[5]+j5;
					  adbl_t[6] = adbl[6] + i4;  bdbl_t[6]=bdbl[6]+j4;
					  adbl_t[7] = adbl[7] + i3;  bdbl_t[7]=bdbl[7]+j3;
					  adbl_t[8] = adbl[8] + i2;  bdbl_t[8]=bdbl[8]+j2;
					  adbl_t[9] = adbl[9] + i1;  bdbl_t[9]=bdbl[9]+j1;
					  X11 = computeIwBconv_mpf_float_100(a_mpf_t,b_mpf_t,nn);
					  boost::multiprecision::float128 elem1 = calcIn16384.compute(adbl_t,bdbl_t);
					  cout <<i1<<","<<i2<<","<<i3<<","<<i4<<"," <<i5<<"," <<i6<<"," <<i7<<"," <<i8<<"," <<i9<<"," <<i10<<"," <<j1<<"," <<j2<<"," <<j3<<"," <<j4<<"," <<j5<<"," <<j6<<"," <<j7<<"," <<j8<<","<< j9<<"," <<j10<<"::" <<X11 << "   " << elem1 <<"   " << (X11.convert_to<float128>()-elem1)/X11.convert_to<float128>() << endl;
					}}}}}}}}}}}}}}}}}}}}

  calcIn16384.unsetPrior();
}
boost::multiprecision::mpf_float_100 compute_sum_f(boost::multiprecision::mpf_float_100 c_0, boost::multiprecision::mpf_float_100 *c, int length){
  boost::multiprecision::mpf_float_100 res;
  res =0 ;
  for(int i = 0; i < length; i++){
    res += c[i]/(boost::math::powm1<boost::multiprecision::mpf_float_100>(0.5,-(c_0 + i))+1);
  }
  return res;
}
void compute_new_coeff_conv(boost::multiprecision::mpf_float_100 &c_0, boost::multiprecision::mpf_float_100 *c,boost::multiprecision::mpf_float_100 a,boost::multiprecision::mpf_float_100 b,int size){
  mpf_float_100 w[size];
  mpf_float_100 res[2*size -1];
  fill_n(res,2*size -1,0.0);
  w[0] = 1.0;
  for(int i =1; i <size; i++)
    {
      w[i] = w[i-1] * (mpf_float_100)(i - b)/(mpf_float_100)i;
    }
  convsse<boost::multiprecision::mpf_float_100>(w,c,size,res);
  c_0 += a;
  for(int i = 0; i < size; i++){
    c[i] = res[i]/(i + c_0);
  }
}

boost::multiprecision::mpf_float_100 computeIwBconv_mpf_float_100(boost::multiprecision::mpf_float_100 *a,boost::multiprecision::mpf_float_100 *b,unsigned size){
  int sec =0 ;
  double long nsec = 0;
  struct timespec tt;
  boost::multiprecision::mpf_float_100 X = 0.0;
  boost::multiprecision::mpf_float_100 A[size+1],B[size+1];
  boost::multiprecision::mpf_float_100 c[N],cInv[N];
  boost::multiprecision::mpf_float_100 c_0,cInv_0;;
  boost::multiprecision::mpf_float_100 aInv[size],bInv[size];
  inverseArray<boost::multiprecision::mpf_float_100>(a,aInv,size);
  inverseArray<boost::multiprecision::mpf_float_100>(b,bInv,size);
  std::fill_n(c,N,0.0);
  std::fill_n(cInv,N,0.0);
  std::fill_n(A,size+1,0.0);
  std::fill_n(B,size+1,0.0);
  A[0] = B[0] = 1.0;
  c[0] = cInv[0] = 1.0;
  c_0 = cInv_0 = 0.0;
  for(int i = 0; i < size; i++){
    compute_new_coeff_conv(c_0,c,a[i],b[i],N);
    if(VERBOSE >= 10){
      for(int j = 0; j < 10; j++)
	cout << c[j] << ",";
      cout << endl;
    }
    A[i+1] = compute_sum_f(c_0,c,N);
    compute_new_coeff_conv(cInv_0,cInv,bInv[i],aInv[i],N);
    B[i+1] = compute_sum_f(cInv_0,cInv,N);
  }
  for(int i = 0; i< size+1;i++)
    X += A[i]*B[size-i];
  return X;
}
main()
{
  testavg();
}
