#ifndef COMPUTEIWB
#define COMPUTEIWB

#include <fftw3.h>
#include <iostream>
#include <vector>
#include <array>
#include <math.h>
#include <time.h>
//#include <boost/math/special_functions/pow.hpp> 
#include <boost/math/special_functions/log1p.hpp> 
//#define N	50
//#define VERBOSE	0
using namespace std;
const int N = 1000;
/*
  template<class T>
  void convolution(T *input1,T *input2,int size,T *res){
  fftwl_complex *out, *prod;
  fftwl_complex *out1;
  fftwl_plan p,p1,ppp;
  out = (fftwl_complex *) fftwl_malloc(sizeof(fftwl_complex) * size + 1);
  out1 = (fftwl_complex *) fftwl_malloc(sizeof(fftwl_complex) * size + 1);
  prod = (fftwl_complex *) fftwl_malloc(sizeof(fftwl_complex) * (size+1)/2 + 1);
  //fftwl_plan_with_nthreads(1);
  p = fftwl_plan_dft_r2c_1d(size,input1,out, FFTW_ESTIMATE);
  p1 = fftwl_plan_dft_r2c_1d(size,input2,out1, FFTW_ESTIMATE);
  fftwl_execute(p);
  fftwl_execute(p1);
  for(int i = 0; i < (size+1)/2; i++){
  prod[i][0] = out[i][0]*out1[i][0] - out[i][1]*out1[i][1];
  prod[i][1] = out[i][1]*out1[i][0] + out[i][0]*out1[i][1];

  if(VERBOSE >= 10){
  std::cout << out[i][0]<<"+i"<<out[i][1]<<"----"<<out1[i][0]<<"+i"<<out1[i][1]; 
  std::cout << "----Product   "<< prod[i][0]<<"+i"<<prod[i][1]<<endl; 
  }
  }
  ppp = fftwl_plan_dft_c2r_1d(size,prod,res, FFTW_ESTIMATE);
  fftwl_execute(ppp);

  fftwl_destroy_plan(p);
  fftwl_destroy_plan(p1);
  fftwl_destroy_plan(ppp);
  fftwl_free(out);
  fftwl_free(out1);
  fftwl_free(prod);

  }
*/
/*
  template<class T>
  void compute_new_coeff(T *c,T a,T b,int size){
  T w[2*size - 1];
  T res[2*size -1];
  T cc[2*size - 1];
  std::fill_n(w + size, size, 0);
  std::fill_n(cc + size, size, 0);
  w[0] = 1;
  for(int i =1; i <size; i++)
  {
  cc[i-1] = c[i];
  w[i] = w[i-1] * (T)(i - b)/(T)i;
  }
  cc[size -1] = c[size];

  if(VERBOSE >=10){
  cout << "w:";
  for(int i = 0 ; i < 2*size - 1;i++)
  cout << w[i] << " ";
  cout << endl;
  cout << "cc:";
  for(int i = 0 ; i < 2*size - 1;i++)
  cout << cc[i] << " ";
  cout << endl;
  }
  convolution<T>(w,cc,2*size -1,res);

  //cout << "res:";
  for(int i = 0 ; i < 2*size - 1;i++){
  res[i] /= (2*size -1);
  //if(VERBOSE>=10)	cout << res[i] << " ";
  }
  //cout << endl;

  c[0] += a;
  for(int i = 1; i <= size; i++){
  c[i] = res[i-1]/(i-1 + c[0]);
  }
  }
*/
template < class T>
T mypow(T base, int p){
  T temp = base;
  if (p==0) return 1;
  if (base == 0) return 0;
  for(int i = 0 ; i <p-1 ; i++)
    temp *= base;
  return temp;
}
/*	
	template <class T>
	T compute_sum(boost::multiprecision::mpq_rational *c, int length){
	T res = 0;
	double x = 0;
	T residue = 0;
	double intpp;
	boost::multiprecision::mpq_rational a(5,10);
	x = modf(c[0].template convert_to<double>(),&intpp);
	residue = (T)pow(0.5,x) ;
	//cout << c[0] << " *** " << c[0].template convert_to<int>()<< "   " << c[0].template convert_to<double>() <<"  "<<x<<"  " <<pow(0.5,x) << "   "<< residue<<"   " << residue.template convert_to<double>()<< endl;
	for(int i = 0; i < length; i++){
	//res += c[i+1]*(T)(powl(0.5,x));
	res += c[i+1]*mypow(a,c[0].template convert_to<int>() + i);
	}
	res *= residue;
	return res;
	}
*/
template <class T>
T compute_sum(T *c, int length){
  T res ;
  const T *c_const = c;
  res =0 ;
  for(int i = 0; i < length; i++){
    const T c_const_1 = c_const[0] + i;
    //	cout << c_const[i]<<"  " <<c_const_1 <<"  " << (T)pow(0.5,c_const_1) << endl;
    res += c_const[i] * (T)pow(0.5,c_const_1);
  }
  return res;
}	
/*
  template <>
  long double compute_sum(long double *c, int length){
  T res = 0;
  for(int i = 0; i < length; i++){
  long double x = i;
  x += c[0];
  res += c[i+1]*(T)(powl(0.5,x));
  }
  return res;
  }
*/
template <class T>
void inverseArray(T *a,T *aInv, unsigned size){
  for(int i = 0; i < size; i++)
    aInv[i] = a[size - i  - 1];
}
/*
  template <class T>
  T computeIwB(T *a,T *b,unsigned size){
  //     int sec =0 ;
  //    double long nsec = 0; 
  //   struct timespec tt;
  T X = 0;
  T A[size+1],B[size+1];
  T c[N+1],cInv[N+1];
  T aInv[size],bInv[size];
  inverseArray<T>(a,aInv,size);
  inverseArray<T>(b,bInv,size);
  std::fill_n(c,N+1,0);
  std::fill_n(cInv,N+1,0);
  std::fill_n(A,size+1,0);
  std::fill_n(B,size+1,0);
  A[0] = B[0] = 1;
  c[1] = cInv[1] = 1;
  // fftwl_init_threads();
  //   t = clock();
  // clock_gettime(CLOCK_REALTIME,&tt);
  //  sec = tt.tv_sec;
  //  nsec = tt.tv_nsec;
  // cout << sec << "," << nsec << endl;

  for(int i = 0; i < size ; i++){
  compute_new_coeff<T>(c,a[i],b[i],N);

  if(VERBOSE >= 10){
  for(int j = 0; j < N+1; j++)
  cout << c[j] << ",";
  cout << endl;
  }

  A[i+1] = compute_sum<T>(c,N);
  compute_new_coeff<T>(cInv,bInv[i],aInv[i],N);
  B[i+1] = compute_sum<T>(cInv,N);
  }
  for(int i = 0; i< size+1;i++)
  X += A[i]*B[size-i];
  // clock_gettime(CLOCK_REALTIME,&tt);
  //cout << tt.tv_sec << "," << tt.tv_nsec << endl;
  // sec = tt.tv_sec -sec;
  //  nsec= tt.tv_nsec - nsec;
  //   nsec = 1e-9 * nsec;
  //    if(nsec < 0 ) {
  //	     if(sec > 0) sec--;
  //	     nsec = 1e+9 + nsec;
  // }
  // cout << "fft double clock " << sec<<"," << nsec << "(seconds)" << endl;
  return X;
  }
*/
#endif
